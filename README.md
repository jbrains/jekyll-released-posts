A Jekyll plugin for marking posts as "released", in case you want to publish a post without including it on the welcome page, archive, or in your RSS feed.

# "Released" Posts

I had a problem. I wanted to publish a post in the future, but I wanted _now_ to use Buffer to schedule a tweet publicizing that post. In order to see the nice, pretty Twitter card for this post on Twitter, Buffer needs to see the post _now_ (to get its featured image and blurb), which means that I need to publish the post; however, I don't want the post to show on the front page of my blog, because although it's published, it's not yet _released_.

Yes, I know.

I'm using the word _released_ to refer to a post once it has been _published_ and is ready to be publicized: included on the front page, in the archives, and so on. A post can be published but not released, but of course if it's released, then it must also be published.

## The Only Meaning of "Released" So Far

So far, _released_ means "published no later than now". Maybe in the future we can extract the notion of _released_ in order to make it easier to change, but for now you'll have to change the Ruby code directly. I don't know how to make this better... yet.

## Use It!

```liquid
{% for post in site.released_posts limit: 10 %}
...all your wonderful HTML to display a single post...
{% endfor %}
```

## Uhh... Use a Filter!

I wanted to use a filter, but as of June 2020, the Liquid template language doesn't support this:

```liquid
{% for post in site.posts | released | limit: 10 %}
```

If there were 10 unreleased posts---maybe I wrote up a storm and wanted to coast for 3 months---then trying to do `for post in site.posts limit: 10 | released` would find 0 posts! That won't do. No matter what, I still want 10 posts on my welcome page. So the generator was the only way I could think to do it.

## Do Better!

If you have a better idea how to solve this problem, then I'd like to hear about it. Submit an issue. Thanks!

