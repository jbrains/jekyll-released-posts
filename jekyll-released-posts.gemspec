Gem::Specification.new do |s|
  s.name = 'jekyll-released-posts'
  s.authors = ["J. B. Rainsberger", "Pat Maddox"]
  s.version = '1.0.1'
  s.summary = "When 'draft' and 'published' isn't enough."
  s.files = ["lib/jekyll-released-posts.rb"]

  s.date = '2020-07-11'
  s.description = "When you want to publish a post, but not promote it yet on your welcome page, you might want to call it 'released'."
  s.email = 'jekyll-released-posts@jbrains.ca'
  s.homepage = 'https://gitlab.com/jbrains/jekyll-released-posts'
  s.license = 'GPL-3.0-only'
end
