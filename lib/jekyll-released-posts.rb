require "jekyll"

# An approximation of Maybe.orElse
# REFACTOR Replace with maddox's better_or_else gem.
# REFACTOR Perhaps replace with https://github.com/alexpeachey/or_else
class Object
  def or_else
    self
  end
end

class NilClass
  def or_else
    yield
  end
end

module JekyllReleasedPosts
  # REFACTOR Move this into some kind of ReleasedPost specialization of Jekyll Document/Post/whatever
  # REFACTOR How to add this as a virtual attribute on Jekyll Post/Document?
  class ReleasedPost
    def initialize(jekyll_document)
      @jekyll_document = jekyll_document
    end

    def released?(as_of: Time.now)
      @jekyll_document.data["released"].or_else { @jekyll_document.date <= as_of }
    end
  end
end

# CONTRACT Something has already defined Jekyll::Site.
# (Good news! If Jekyll doesn't do it, we're fucked.)
Jekyll::Drops::SiteDrop.class_eval do
  # REFACTOR Replace this with a method that computes on the fly
  def released_posts
    # CONTRACT self.posts already only selects published posts (or so we believe)
    # REFACTOR Move this into a filter on any array of Document objects and not only site.posts
    posts.select { | document |
      JekyllReleasedPosts::ReleasedPost.new(document).released?(as_of: Time.now)
    }
  end
end
