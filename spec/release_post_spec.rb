require "jekyll-released-posts"

describe "Deciding whether a post is released" do
  describe "post not marked as released" do
    let(:data_without_released) { {} }

    example "published in the past" do
      published_in_the_past = double("a Jekyll Document", data: data_without_released, date: Time.parse("2020-07-11 19:06 -0300"))
      expect(JekyllReleasedPosts::ReleasedPost.new(published_in_the_past).released?(as_of: Time.parse("2020-07-11 19:07 -0300"))).to be(true)
    end

    example "published in the future" do
      published_in_the_future = double("a Jekyll Document", data: data_without_released, date: Time.parse("2020-07-11 19:08 -0300"))
      expect(JekyllReleasedPosts::ReleasedPost.new(published_in_the_future).released?(as_of: Time.parse("2020-07-11 19:07 -0300"))).to be(false)
    end
  end

  describe "post explicitly marked as released or not" do
    example "marked as released in spite of being published in the future" do
      explicitly_marked_released = double("a Jekyll Document", data: {"released" => true}, date: Time.parse("2020-07-11 19:08 -0300"))
      expect(JekyllReleasedPosts::ReleasedPost.new(explicitly_marked_released).released?(as_of: Time.parse("2020-07-11 19:07 -0300"))).to be(true)
    end

    example "marked as _not_ released in spite of being published in the past" do
      explicitly_marked_not_for_release = double("a Jekyll Document", data: {"released" => false}, date: Time.parse("2020-07-11 19:06 -0300"))
      expect(JekyllReleasedPosts::ReleasedPost.new(explicitly_marked_not_for_release).released?(as_of: Time.parse("2020-07-11 19:07 -0300"))).to be(false)
    end
  end
end
